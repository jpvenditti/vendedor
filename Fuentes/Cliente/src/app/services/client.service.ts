import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Client } from '../models/client.model';

import { delay } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})

export class ClientService {

  constructor( private http: HttpClient ) { }

  getClients() {

    return  this.http.get<Client[]>( 'https://dacs2019-dist.firebaseio.com/clients.json' )
              .pipe(
                delay( 2500 )
              );

  }
}