export interface Client{
    id: number;
    dni: number;
    name: String;
    surname: String;
    cell_phone: number;
    email: String;
    address: Address;
}

interface Address {
    latitude: number;
    longitude: number;
}
