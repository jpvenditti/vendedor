import { Component } from '@angular/core';
import { Client } from '../models/client.model';
import { ClientService } from '../services/client.service';
import { Router, NavigationExtras } from '@angular/router';
import { ParseError } from '@angular/compiler';


@Component({
  selector: 'app-client',
  templateUrl: 'client.page.html',
  styleUrls: ['client.page.scss']
})
export class ClientPage {

  clients: Client[] = [];
  searchText = '';

  constructor( private clientService: ClientService, private router: Router) {
    this.clientService.getClients().subscribe( resp => this.clients = resp );
  }

  showDetails(client) {
    
    let clientDetails: NavigationExtras = {
      queryParams: {
        special: JSON.stringify(client)
      }
    };
    this.router.navigate(['client-details'], clientDetails);
  }

}
